function getAllList(evt,tabName){
  
  
  var request = new XMLHttpRequest()

  request.open('GET', 'http://localhost:8080/lancamentos', true)

  request.onload = function() {
    var data = JSON.parse(this.response)
    
    if (request.status >= 200 && request.status < 400) {
      drawTableLancamento(data, tabName)
    } else {
      console.log('error')
    }
  }
  
  request.send()
  
  openTab(evt, tabName)
}

function getMeses(evt, tabName){
  const app = document.getElementById("porCategoria")

  const container = document.createElement('div')
  container.className = 'container'
  container.id = 'containerMeses'

  var request = new XMLHttpRequest()
    
  request.open('GET', 'http://localhost:8080/lancamentos/meses', true)
  request.onload = function() {
    // Begin accessing JSON data here
    var data = JSON.parse(this.response)
  
    if (request.status >= 200 && request.status < 400){
      data.forEach(mes => {
        mes = JSON.parse(mes)
        const p = document.createElement('p')
        p.textContent = this.key
        container.appendChild(p)

        const containermes = document.createElement('div')
        containermes.className = 'container'
        containermes.id = 'container'+this.key
        container.appendChild(containermes)
        
        drawTableLancamento(this, containercategoria.id)
        
      })
    }
     else {
      console.log('error')
    } 
  }
  app.appendChild(container)
  request.send()
  openTab(evt, tabName)
}

function getCategorias(evt, tabName){
  const app = document.getElementById("porCategoria")

  const container = document.createElement('div')
  container.className = 'container'
  container.id = 'containerCategorias'

  var request = new XMLHttpRequest()
    
  request.open('GET', 'http://localhost:8080/categorias', true)
  request.onload = function() {
    // Begin accessing JSON data here
    var data = JSON.parse(this.response)
  
    if (request.status >= 200 && request.status < 400) {
      data.forEach(categoria => {

        const p = document.createElement('p')
        p.textContent = categoria.nome
        requestLancamento = new XMLHttpRequest()
        container.appendChild(p)  
        const containercategoria = document.createElement('div')
        containercategoria.className = 'container'
        containercategoria.id = 'container'+categoria.nome
        container.appendChild(containercategoria)

        requestLancamento.open('GET', 'http://localhost:8080/lancamentos?categoria='+categoria.id, true)
        requestLancamento.onload = function() {
          var lancamentos = JSON.parse(this.response)
          if (request.status >= 200 && request.status < 400) {
            drawTableLancamento(lancamentos, containercategoria.id)
          }
          else{
            console.log('error')
          }

        }
        requestLancamento.send()
      })
    } else {
      console.log('error')
    } 
  }
  app.appendChild(container)
  request.send()
  openTab(evt, tabName)
}


function openTab(evt, tabName) {
  
  var i, tabcontent, tablinks

  
  tabcontent = document.getElementsByClassName('tabcontent')
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = 'none'
  }

  tablinks = document.getElementsByClassName('tablinks')
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(' active', '')
  }

  document.getElementById(tabName).style.display = "block";
  evt.currentTarget.className += ' active'
}

function drawTableLancamento(data, tabName){
  const app = document.getElementById(tabName)

  const table = document.createElement("table");
  table.className = "table";
  table.id = "tbLancamentos_"+tabName;
  const tbody = document.createElement("tbody");
  const thead = document.createElement("thead");
  var tr = document.createElement("tr");
  var th = document.createElement("th");

  th.scope = "col";
  th.textContent = "Valor";
  tr.appendChild(th);
  
  th = document.createElement("th");
  th.textContent = "Origem";
  tr.appendChild(th);
  
  th = document.createElement("th");
  th.textContent = "Categoria";
  tr.appendChild(th);

  th = document.createElement("th");
  th.textContent = "Mês de Lançamento";
  tr.appendChild(th);

  thead.appendChild(tr);
  table.appendChild(thead);

  data.forEach(lancamento => {
    var tr = document.createElement("tr");
    var td = document.createElement("td");
    
    td.innerText = "R$ "+ lancamento.valor;
    tr.appendChild(td);
   
    td = document.createElement("td");
    td.innerText = lancamento.origem;
    tr.appendChild(td);

    td = document.createElement("td");
    td.innerText = lancamento.categoria;
    tr.appendChild(td);

    td = document.createElement("td");
    td.innerText = lancamento.mes_lancamento;
    tr.appendChild(td);

    tbody.appendChild(tr);
  })

  table.appendChild(tbody)
  app.appendChild(table)
  console.log(app)

}